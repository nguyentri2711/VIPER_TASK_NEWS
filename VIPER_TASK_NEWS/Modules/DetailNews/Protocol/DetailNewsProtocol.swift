//
//  DetailNewsProtocol.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

protocol DetailNewsPresenterInputProtocol: class {
    // VIEW --> PRESENTER
    func getDataNews()
}

protocol  DetailNewsInteractorInputProtocol: class {
    // PRESENTER --> INTERACTOR
    func getDataNews()
}

protocol DetailNewsPresenterOutputProtocol: class{
    // INTERACTOR --> PRESENTER
    func setDataNews(news: NewsModel)
}

protocol DetailNewsViewProtocol: class{
    // PRESENTER --> VIEW
    func showDataNews(news: NewsModel)
}

