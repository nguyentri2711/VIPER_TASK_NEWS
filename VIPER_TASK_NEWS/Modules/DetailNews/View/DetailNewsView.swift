//
//  DetailNewsView.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import UIKit

class DetailNewsView: UIViewController {
    
    var detailNewsPresenter: DetailNewsPresenterInputProtocol?

    @IBOutlet weak var tblDetailNews: UITableView!
    
    var newsModel: NewsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDetailNews.allowsSelection = false
        tblDetailNews.separatorStyle = .none
        detailNewsPresenter?.getDataNews()
    }
    deinit {
        print("deinit DetailNewsView")
    }
}

extension DetailNewsView: DetailNewsViewProtocol{
    
    func showDataNews(news: NewsModel) {
        self.newsModel = news
    }
    
}

extension DetailNewsView: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            let cell = Bundle.main.loadNibNamed("TitleViewCell", owner: self, options: nil)?.first as! TitleViewCell
            cell.titleViewCell.text = newsModel?.getTitle()
            return cell
            
        }else if indexPath.row == 1{
            
            let cell = Bundle.main.loadNibNamed("TimeViewCell", owner: self, options: nil)?.first as! TimeViewCell
            cell.timeViewCell.text = newsModel?.getTime()
            return cell
            
        }else if indexPath.row == 2{
            
            let cell = Bundle.main.loadNibNamed("ContentViewCell", owner: self, options: nil)?.first as! ContentViewCell
            cell.contentViewCell.text = newsModel?.getHead()
            return cell
            
        }else if indexPath.row == 3{
            
            let cell = Bundle.main.loadNibNamed("ImageViewCell", owner: self, options: nil)?.first as! ImageViewCell
            cell.imgViewCell.image = UIImage(named: (newsModel?.getImage())!)
            return cell
            
        }
        
        let cell = Bundle.main.loadNibNamed("ContentViewCell", owner: self.tblDetailNews, options: nil)?.first as! ContentViewCell
        cell.contentViewCell.text = newsModel?.getContent()
        return cell
    }
    

    
}
