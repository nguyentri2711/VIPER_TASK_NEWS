//
//  ImageViewCell.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/13/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import UIKit

class ImageViewCell: UITableViewCell {

    
    @IBOutlet weak var imgViewCell: UIImageView!
    
}
