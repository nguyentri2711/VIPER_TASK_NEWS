//
//  Protocol.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation

protocol ListNewsPresenterInputProtocol: class {
    // VIEW --> PRESENTER
    func getDataListNews()
    func showDetailNews(news: NewsModel)
}

protocol ListNewsInteractorInput: class {
    // PRESENTER --> INTERACTOR
    func getDataListNews()
}

protocol ListNewsPresenterOutputProtocol: class {
    // INTERACTOR --> PRESENTER
    func setDataListNews(listNews: [NewsModel])
}

protocol ListNewsViewProtocol: class {
    // PRESENTER --> VIEW
    func showDataListNews(listNews: [NewsModel])
}

protocol ListNewsWireframeProtocol: class {
    // PRESENTER --> WIREFRAME
    func showDetailNewsView(fromView: ListNewsViewProtocol, news: NewsModel)
}
