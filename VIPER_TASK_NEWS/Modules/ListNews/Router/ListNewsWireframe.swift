//
//  ListNewsWireframe.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import Foundation
import UIKit

class ListNewsWireframe{
    
    func getCreateModule() -> UIViewController{
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navController = storyboard.instantiateViewController(withIdentifier: "ListNewsNavigationController")
        let view = navController.childViewControllers.first as? ListNewsView
        
        let presenter = ListNewsPresenter()
        let interactor = ListNewsInteractor()
        
        view?.listNewsPrensenter = presenter
        presenter.listNewsView = view
        presenter.wireFrame = self
        
        presenter.listNewsInteractor = interactor
        interactor.listNewsPresenter = presenter
        
        
        return navController
    }
    
    
    
}

extension ListNewsWireframe: ListNewsWireframeProtocol{
    
    func showDetailNewsView(fromView: ListNewsViewProtocol, news: NewsModel){
        // do show detail
        let detailWireframe = DetailNewsWireframe()
        let view = detailWireframe.getCreateModule(news: news)
        if let sourceView = fromView as? UIViewController {
            sourceView.navigationController?.pushViewController(view, animated: true)
        }
    }
    
}
