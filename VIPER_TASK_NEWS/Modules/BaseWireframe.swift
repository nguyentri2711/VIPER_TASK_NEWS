//
//  BaseWireframe.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import UIKit

class BaseWireFrame{
    class func createMainModule() -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController")
        return viewController
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}



